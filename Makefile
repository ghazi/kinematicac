# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#	Makefile for Unix & Linux Systems	 #
#	using a GNU C++ compiler	 #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# compiler flags
#	-g	--Enable debugging
#	-Wall	--Turn on all warnings
#	-D_USE_FIXED_PROTOTYPES_
#	 --Force the compiler to use the correct headers
#	-ansi	--Don't use GNU ext; do use ansi standard.

CXX = g++
CXXFLAGS = -O3 -ansi -Wall
DEBUGFLAGS = -g -ansi -Wall
INCLUDE = -I./src
INCLUDECXX = -I../src
LIBS = -L./build

VPATH = src:build:build/lib

velobjects = Rotation.o Point.o Velpoint.o
addobjects = Rotation.o Point.o Adder.o
getobjects = Rotation.o Point.o Getrot.o
intobjects = Rotation.o Point.o Intertec.o Line.o
rotobjects = Rotation.o Point.o Rotpoint.o

all:			adder velpoint getrot intertec rotpoint

%.o: %.cpp 
	$(CXX) -c $(CFLAGS) $(INCLUDE) $< -o build/$@ 


velpoint:		$(velobjects)
			cd build;$(CXX) $(CXXFLAGS) $(INCLUDECXX) $(LIBS) ../Velpoint-main.cpp $(velobjects) -o ../bin/velpoint
rotpoint:		$(rotobjects)
			cd build;$(CXX) $(CXXFLAGS) $(INCLUDECXX) $(LIBS) ../Rotpoint-main.cpp $(rotobjects) -o ../bin/rotpoint
adder:			$(addobjects)
			cd build;$(CXX) $(CXXFLAGS) $(INCLUDECXX) $(LIBS) ../Adder-main.cpp $(addobjects) -o ../bin/adder
getrot:			$(getobjects)
			cd build;$(CXX) $(CXXFLAGS) $(INCLUDECXX) $(LIBS) ../Getrot-main.cpp $(getobjects) -o ../bin/getrot

intertec:		$(intobjects)
			cd build;$(CXX) $(CXXFLAGS) $(INCLUDECXX) $(LIBS) ../Intertec-main.cpp $(intobjects) -o ../bin/intertec

test:			Rotation.o
			cd build;$(CXX) $(CXXFLAGS) $(INCLUDECXX) $(LIBS) ../test.cpp $(intobjects) -o ../bin/test

clean:
			rm -f build/*.o
			rm -f src/*.o
