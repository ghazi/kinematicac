/*
Getrot - A program for calculating and interpolating rotations
part of the Kinematica suite
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Getrot.cpp is part of the Getrot program

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Getrot.cpp - This file
 *	The class for handling the rotation file interaction
 *      Version 2.0 April 24th, 2013
 *
 **/



#include "Getrot.h"


/**********************************************************************/
/***************** CONSTRUCTORS/DESTRUCTORS ***************************/
/**********************************************************************/


Getrot::Getrot(const char * rotfile,double  t,int id1,int id2) {

/******** 
	* This Constructor reads in the rotation file
	* and stores it in a list of Rotations
	* 
	******/

	
	time = t;
	plate1 = id1;
	plate2 = id2;

	// Open files
		
	finrot.open(rotfile);
	
	// Read Through Rotation File and Create List
	int plate,cp;
        double lat,lon,ang,time;
	std::string in_line;
	while (std::getline(finrot,in_line)) { //finrot >> p) {
		std::stringstream line(in_line);
		line >> plate >> time >> lat >> lon >> ang >> cp;
                if (plate == 999) {
                        continue;
                }
                //finrot >> time >> lat >> lon >> ang >> cp;
                Rotation r(lon,lat,ang,plate,cp,time);
                rot.insert(rot.end(),r);
        }


} // Getrot()


Getrot::~Getrot() {

/********
	* The Destructor closes the file stream 
	*
	****/
	finrot.close();
}

/**********************************************************************/
/**************** PRIVATE MEMBER FUNCTIONS ***************************/
/**********************************************************************/


list <Rotation> Getrot::giventime(double & time) {

/********
	* This Function reads the Rotation List and Outputs
	* a list of all Rotations for a specific time
	* 
	****/

Rotation current(0.0,90.0,0.0,999,999,0.0);
Rotation previous(0.0,90.0,0.0,999,999,0.0);
list <Rotation> output;
int plate = 999;
for (list<Rotation>::iterator update = rot.begin();
			update != rot.end();
			++update) {
	current = *update;
	if ((current.getplate() == 999) || (current.getplate() == plate)) {
		continue;
	}
	else if (current.gettime() == time) {
			output.insert(output.end(), current);
			plate = current.getplate();
			previous = current;
			
	}

	else if (current.gettime() < time) {
			if ((previous.getplate() != 999) && 
			(previous.getplate() != plate) && 
			(previous.getplate() != current.getplate())) {
				previous.settime(time);
				output.insert(output.end(),previous);
				plate = previous.getplate();
			}
			previous = current;
			
	}

	else {
			double t1 = previous.gettime();
			double t2 = current.gettime();
			double delta = ((time - t1)/(t2 - t1));
			Rotation * ans = previous.slerp(current,delta);
			output.insert(output.end(),*ans);
			plate = current.getplate();
			previous = current;
	}
} // for loop

if  (current.gettime() < time) {
				current.settime(time);
				output.insert(output.end(),current);
}

return output;

} // giventime()

Rotation Getrot::getrot(double & time,int & plate, int & cplate) {

/********
	* This method calls the giventime() method
	* finds the two appropriate absolute rotations
	* swaps the plate pairs and adds them
	*
	*******/

list <Rotation> results = giventime(time);
list <Rotation> final;

Rotation current;
int cplateid;
int plateid;

// Check to see if rotation for plate pair already exists

for (list<Rotation>::iterator update = results.begin();
			update != results.end();
			++update) {   

	current = *update;
	plateid = current.getplate();
	cplateid = current.getcplate();
	// If so, return that plate pair
	if ((plate == plateid)&&(cplate == cplateid)) {
		current.swap();
		current.toNP();
		return current;
	}

	if ((plate == cplateid)&&(cplate == plateid)) {
		current.toNP();
		return current;
	}
}
 
for (list<Rotation>::iterator update = results.begin();
			update != results.end();
			++update) {    // OUTER LOOP
			
		current = *update;
		cplateid = current.getcplate();

		if (cplateid == 000) {
				final.insert(final.end(),current);
				continue;
		}
		list<Rotation>::iterator rotfnd = results.begin();
		int num = 0; // number of searches through list	
	
	while((cplateid != 000) && (num < 2)) {		// INNER LOOP
		// Start at the beginning, but loop through only once (num <2)
		if (rotfnd == results.end()) {
				rotfnd = results.begin();
				num++;
		}
		Rotation inner = *rotfnd;	
		if (inner.getplate() == cplateid) {
				Rotation tmp = current + inner;
				current = tmp; 
				cplateid = current.getcplate();
				num = 0;
		}

			
		rotfnd++;
	}

	if (num < 2) final.insert(final.end(),current);
}

Rotation r1,r2,ans;
bool found1 = false;
bool found2 = false;

for (list<Rotation>::iterator update = final.begin();
			update != final.end();
			++update) {

	Rotation current = *update;
	if (plate == current.getplate()) {
				
				r1 = current;
				if (cplate == 000) {
						r1.swap();
						r1.toNP();
						return r1;
				}
				found1 = true;
	}
	else if (cplate == current.getplate()) {
				
					r2 = current;

				if (plate == 000) {
						r2.toNP();
						return r2;
				}
						 
				
				found2 = true;
	}

}

if (found1 && found2) {
			r1.swap();
			ans = r2 + r1;
			ans.toNP();
}

else {
	cerr << "Warning: Rotation File Error, Plate Pair not Found!\n";
	cerr << "for plate " <<plate <<" and conjugate plate " <<cplate  <<endl;
	Rotation tmp(0.0,90.0,0.0,999,999,0.0);
	ans = tmp;	
	
	
}

return ans;

} // getrot();

/**********************************************************************/
/***************** PUBLIC MEMBER FUNCTIONS ****************************/
/**********************************************************************/


int Getrot::run(){

/********
	* This is the only public method for this class
	* It interpolates between all this isochrons and reconstructs
	* by calling the appropriate methods
	*
	*******/

	Rotation answer = getrot(time,plate1,plate2);
	//answer.setXY();
	cout << answer;
	return 0;


} // run
