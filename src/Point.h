/*
The Point class
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Point.h is part of the Kinematica suite

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Point.h - This file
 *	The main class for handling point data in latitude/longitude
 *      Version 2.0 April 24th, 2013
 *
 **/


#ifndef _POINT_H_
#define _POINT_H_

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

class Point {
	
	protected:
	
		// Polar Co-ordinates for Point
		// NOTE: (Longitude,Latitude) configuration

		double lon;
		double lat;
	
		bool fisGMTPoint;
		bool fisXYoutput;

		// Data for added GMT information

		double age;
		double spreadrate;
		double spreaddir;
		double spreadasym;
		double vb(double x) const;

	public:
		// Constructors
		Point();
		Point(double X,double Y);
		Point(double X,double Y,double t,double sr,double sd,double sa);		// Print Members
        	double getlon() const {return lon;};
		double getlat() const {return lat;};
		double getage() const {return age;};
		double getspreaddir() const {return spreaddir;};
		double getspreadasym() const {return spreadasym;};
		double getspreadrate() const {return spreadrate;};


		// Miscellaneous

		void equals(Point const &p);
		void shift();

		// Distance and Course Methods

		double dist(Point const &p) const; // returns distance in minutes
		double cour(Point const &p) const; // course in degrees along a loxodrome

		// Boolean Operators

		bool operator==(const Point &p) const; // (lat then lon)
		bool operator<(const Point &p) const; // (Only by lat)

		// Printing

		friend ostream & operator<<(ostream & os, const Point &p);
		
		// GMT routines
	
		void setXY(bool XY=true) {fisXYoutput = XY;};
		bool isXYoutput() const {return fisXYoutput;};
		bool isGMTPoint () const {return fisGMTPoint;};
		void update(double t,double sr,double sd,double sa);
		void update(Point &p);
};
#endif
