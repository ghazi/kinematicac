/*
Velpoint - A Program for calculating velocities of points
Velpoint is part of the Kinematica suite
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Velpoint.cpp is part of Velpoint
    Velpoint is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Velpoint.cpp - This file
 *	The main class for handling velocity and distance calculations
 *      Version 2.0 April 24th, 2013
 *
 **/


#include <iterator>  // for std::ostream_iterator
#include <string>
#include "Velpoint.h"

/**********************************************************************/
/***************** CONSTRUCTORS/DESTRUCTORS ***************************/
/**********************************************************************/


Velpoint::Velpoint(Point p, Rotation r, double t, bool ft,bool verb) {

/******** 
	* 
	******/

	point = p;
	rotation = r;
	time = t;
	ftime = ft;
	fverbose = verb;

} // Velpoint()


/**********************************************************************/
/**************** PRIVATE MEMBER FUNCTIONS ***************************/
/**********************************************************************/




/**********************************************************************/
/***************** PUBLIC MEMBER FUNCTIONS ****************************/
/**********************************************************************/


int Velpoint::run(){

/********
	* This is the only public method for this class
	* It calculates the distance or velocity of a point
	* given a rotation (either finite or stage)
	* depending on whether the time is included or not when calling the function
	*******/
Point q = rotation*point;
// 3 decimal places
cout << fixed << setprecision(3);
// time variable in million years; velocity in cm/yr
if(fverbose) {
	cout << "Original point: "<< point << "\n";
	cout << "Final Point: " << q << "\n";
}

if (ftime) {
	if (time == 0.0) {
		if (fverbose) {
			cout << "Time is set to 0 and therefore velocity is undefined " << "\n"; 
			cout << "Bearing: " << point.cour(q) << "\n"; 
		}
		else {
			cerr << "nan " << point.cour(q) << "\n";
		}
	}
	else {
	// PRE: time != 0
	double velocity = point.dist(q) / (time*10.0); // Convert km/Myr to cm/yr
		if (fverbose) {
			cout << "Velocity (cm/yr): " << velocity << "\n"; 
			cout << "Bearing: " << point.cour(q) << "\n"; 
		}
		else {
			cout << velocity << " " << point.cour(q) << "\n";
		}
	}
}
else {
	if (fverbose) {
		cout << "Distance (km): " << point.dist(q) << "\n"; 
		cout << "Bearing: " << point.cour(q) << "\n"; 
	}
	else {
		cout << point.dist(q) << " " << point.cour(q) << "\n";
	}
}
return 0;
} // run


