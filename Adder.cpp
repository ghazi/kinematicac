/*
Adder - A Program for calculating velocities of points
Adder is part of the Kinematica suite
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Adder.cpp is part of Adder
    Adder is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Adder.cpp - This file
 *	The main class for handling velocity and distance calculations
 *      Version 2.0 April 24th, 2013
 *
 **/


#include <iterator>  // for std::ostream_iterator
#include <string>
#include "Adder.h"

/**********************************************************************/
/***************** CONSTRUCTORS/DESTRUCTORS ***************************/
/**********************************************************************/


Adder::Adder(Rotation r1, Rotation r2, bool verb) {

/******** 
	* 
	******/

	rotationA = r1;
	rotationB = r2;
	fverbose = verb;

} // Adder()


/**********************************************************************/
/**************** PRIVATE MEMBER FUNCTIONS ***************************/
/**********************************************************************/




/**********************************************************************/
/***************** PUBLIC MEMBER FUNCTIONS ****************************/
/**********************************************************************/


int Adder::run(){

/********
	* This is the only public method for this class
	* It calculates the distance or velocity of a point
	* given a rotation (either finite or stage)
	* depending on whether the time is included or not when calling the function
	*******/
Rotation C = rotationA + rotationB;
// 3 decimal places
cout << fixed << setprecision(3);
// time variable in million years; velocity in cm/yr
cout << C.getlat() << " " << C.getlon() << " " << C.getang() << "\n";
return 0;
} // run


