/*
The Rotation class
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Rotation.cpp is part of the Kinematica suite

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Rotation.cpp - This file
 *	The main class for handling spherical rotations in Euler pole/angle or as quaternions. 
 *      Version 2.0 April 24th, 2013
 *
 **/

#include <cmath>
#include "Rotation.h"
#ifndef rad
#define rad 0.0174532925199432958
#endif
#ifndef deg
#define deg 57.2957795
#endif

Rotation::Rotation() : Point(0.0,90.0){

	plate = 000;
	cplate = 002;
	time = 0.0;
	theta = 0.0;
	setquat();
}

Rotation::Rotation(double X,double Y,
		double ang,int p,int cp,double t) : Point(X,Y) {
	plate = p;
	cplate = cp;
	time = t;
	theta = ang;
	setquat();
	fisstage = false;
	timeend = -999.0;

}


Rotation::Rotation(double * q,int p,int cp,double t)  {

	quat[0] = q[0];
	quat[1] = q[1];
	quat[2] = q[2];
	quat[3] = q[3];
	setpolar();
	plate = p;
	cplate = cp;
	time = t;
	fisstage = false;
	timeend = -999.0;

}

Rotation::Rotation(double X,double Y,double ang,int p,int cp,double t,double te) : Point(X,Y) {

	plate = p;
	cplate = cp;
	time = t;
	theta = ang;
	setquat();
	timeend = te;
	fisstage = true;
}


Rotation::~Rotation() {


}

/**********************************************************************/
/***************** PUBLIC MEMBER FUNCTIONS ****************************/
/**********************************************************************/


/********
	* part() is used to multiple the angle by a fraction
	* required when interpolating rotations
	*
	******/

void Rotation::part(double d,double t) {

theta *= d;
time = t;
setquat();
return;
}

Rotation * Rotation::slerp(const Rotation r,const double t) {

/********
	* slerp() interpolates between two rotations using the parameter t
	* to determine the linear spherical interpolation between the current
	* rotation and rotation r
	*
	******/
if ((t > 1) || (t < 0)) {
	return new Rotation();
}
double q[4];
double age;
//PRE: quat[i] and r.quat[1] in range [-1,1] hence acos within range subject to rounding errors
double inner = quat[0]*r.quat[0]+quat[1]*r.quat[1]+quat[2]*r.quat[2]+quat[3]*r.quat[3];
//POST: inner can be slightly over 1.0 after rounding errors. 

if (inner > 1.0) {
	inner = 1.0;
}
//POST: inner in range [0,1.0] 
double psi = acos(inner);
//POST: psi in range [0,2*pi]
double ratioA,ratioB;
if (fabs(psi < 0.00001)) {
	ratioA = 0.5;
	ratioB = 0.5;
}
else {
	ratioA = sin((1-t)*psi)/sin(psi);
	ratioB = sin(t*psi)/sin(psi);
}	
q[0] = quat[0]*ratioA + r.quat[0]*ratioB;
q[1] = quat[1]*ratioA + r.quat[1]*ratioB;
q[2] = quat[2]*ratioA + r.quat[2]*ratioB;
q[3] = quat[3]*ratioA + r.quat[3]*ratioB;
age = (1 - t)*time + t*r.time;
double * qu = q;
Rotation * ans = new Rotation(qu,plate,cplate,age);
return ans;
}

/********
	* swap() swaps the moving and fixed plates
	*
	******/

void Rotation::swap() {

int tmp = cplate;
cplate = plate;
plate = tmp;
theta = -theta;
setquat();
}

void Rotation::toNP() {

/********
	* Convert Rotation to equivalent NP Rotation
	*
	*******/

if (lat < 0) {
	lat = -lat;
	lon = 180.00 + lon;
	theta = -theta;
	setquat();
}

}

void Rotation::withinRange() {

/********
	* ensure polar coordinates are within correct ranges
	*
	*******/
	while (lon > 360.0) {
		lon -= 360.0;
	}
	while (lon < -360.0) {
		lon += 360.0;
	}
}	


Rotation Rotation::getNP() {

/********
	* Returns the rotation necessary to transfer
	* Rotation Pole to North Pole
	*
	*******/

double NPlat = 0.000001;
double NPlon = lon - 90;
if (NPlon < 180) NPlon += 360;
if (NPlon > 180) NPlon -= 360;
double ang = 90 - lat;
int cp = 000;
return Rotation(NPlon,NPlat,ang,plate,cp,time);
 
}

bool Rotation::operator==(Rotation &r) {

/********
	* Equality only by fixed and moving plates
	*
	******/

	if ((plate == r.plate) && (cplate == r.cplate)) return true;

return false;

}

bool Rotation::operator<(Rotation &r) {

/********
	* Sorts by moving/fixed plates and then time
	*
	******/

	if (plate < r.plate) return true;
	if (plate > r.plate) return false;
	if (cplate < r.cplate) return true;
	if (cplate > r.cplate) return false;
	if (time < r.time) return true;
	if (time > r.time) return false;
return false;
}


Rotation Rotation::operator-() {

/********
	* sets theta negative only
	* N.B.: Do not use this to change fixed and moving plates, see swap()
	*
	******/

	return Rotation(lon,lat,-theta,plate,cplate,time);
}



Rotation Rotation::operator-(Rotation & r) {

/********
	* Adds two Rotations one with the sign reversed
	* Only valid for calculating Stage Rotations
	*
	*****/
 
	Rotation r1 = *this;
	Rotation r2 = -r;

	Rotation q = r1 + r2;
	
	q.time = time;
	q.timeend = r.time;
	q.fisstage = true;
	
	return q;
}



Rotation Rotation::operator+(Rotation & r) {

/********
	* Adds two rotations
	* Only use this for adding rotations where the first
	* Rotation's fixed plate is the same as the second
	* Rotation's moving plate.
	* N.B.: This is not a symmetrical operator
	*
	*******/
	// For the same pole, we need only add the angles together (and the operator is symmetric)
	/*
	if ((getlon() == r.getlon()) && (getlat() == r.getlat())) {
		double sumlon = getlon();
		double sumlat = getlat();	
		double ang = theta + r.theta;
		cout << "ang/thetas: " << ang << " " << theta << " " << r.theta << "\n";
		double time1;
		time < r.time ? time1 =  r.time : time1 = time; 
		Rotation sum(sumlon,sumlat,ang,plate,r.cplate,time1);
		sum.fisstage = false;
		sum.toNP();
		return sum;
	}
	else {
	*/
	// Otherwise, perform a quaternion addition (and the operator is not symmetric)
		Rotation sum;
		sum.quat[0] = quat[0]*r.quat[0] - quat[1]*r.quat[1] - quat[2]*r.quat[2] - quat[3]*r.quat[3];
		sum.quat[1] = quat[1]*r.quat[0] + quat[0]*r.quat[1] + quat[3]*r.quat[2] - quat[2]*r.quat[3];
		sum.quat[2] = quat[2]*r.quat[0] + quat[0]*r.quat[2] + quat[1]*r.quat[3] - quat[3]*r.quat[1];
		sum.quat[3] = quat[3]*r.quat[0] + quat[0]*r.quat[3] + quat[2]*r.quat[1] - quat[1]*r.quat[2];
		sum.setpolar();
		sum.plate = plate;
		sum.cplate = r.cplate;
		time < r.time ? sum.time = r.time : sum.time = time;
		sum.timeend = -999.0;
		sum.fisstage = false;
		sum.toNP();
		return sum;
	 // } // else different rotation poles
}

Point Rotation::operator*(Point &p) {

/********
	* This utilises the multiplication operator to Rotate points
	* N.B.: This operator is symmetrical
	*
	******/


double plon = getlon();
double plat = getlat();
double rot = theta;
double alon = p.getlon();
double alat = p.getlat();


if (theta == 0) return p;

if (alat == 90.0) alat = 89.9999;

double ft = (alon - plon)/deg;
double sd = sin(ft);
double cd = cos(ft);
ft = alat/deg;
double sv = sin(ft);
double cv = cos(ft);
ft = plat;
if (ft > 89.9999) ft = 89.9999;
if (ft < -89.9999) ft = -89.9999;
ft = ft/deg;
double sp = sin(ft);
double cp = cos(ft);
double cosr = sv * sp + cv * cp * cd;
double sint = cv * sd * cp;
double cost = sv - cosr * sp;
ft = -rot/deg ;
double sr = sin(ft);
double cr = cos(ft);
double sint1 = sint * cr + cost * sr;
double cost1 = cost * cr - sint * sr;
double sx = sp * cosr + cost1;
double sinrk = 1 - sx*sx;
if (sinrk < 0) sinrk = 0;
double cx = sqrt(sinrk);
double rlat = deg * atan2(sx,cx);
double rlon = plon + deg * (atan2(sint1,cosr - sp*sx));
Point rot2(rlon,rlat);
rot2.update(p);
return rot2;

}

Point operator*(Point &p,Rotation &r) {

return (r*p);

}

/**********************************************************************/
/**************** PRIVATE MEMBER FUNCTIONS ***************************/
/**********************************************************************/

void Rotation::setpolar() {

/********
	* Set polar co-ordinates given quaternion
	* Note the relationship between Euler rotation expressed as 
	* direction cosines (alpha, Bx, By, Bz) and (lat,lon,theta)
	* alpha = theta
	* lat = 90 - Bz
	* lon = atan2(cos(By),cos(Bx))
	******/
	
	// PRE: require q0 in the set [-1,1]
	if ((quat[0] >= 1)||(quat[0] <= -1)) {
		 theta = 0;
	}
	else {
		theta = 2*acos(quat[0]);
	}
	// POST: theta in set [0,2*pi]
	// PRE: q3 <= sin(theta/2)
	//      theta != 0	
	if ( (theta == 0)|| (abs(quat[3]) > abs(sin(theta/2)))) { 
		if (((quat[3] >= 0)&&(theta >=0))||((quat[3] <0) && (theta < 0))) {
			lat = 90*rad;
		}
		else {
			lat = -90*rad;
		}
	}
	else {
		lat = asin(quat[3]/sin(theta/2));
	}
	// POST lat in set [-pi/2,pi/2]
	// PRE: quat[1] != 0
	if (quat[1] == 0) {
		lon = 0;
	}
	else {
		lon = atan2(quat[2],quat[1]);
	}
	// POST: lat in set [-pi,pi]
	theta /= rad;
	lat /= rad;
	lon /= rad;
	// POST: lon in [-180,180],theta in [0,360] and lat in [-90,90]
}


void  Rotation::setquat(){

/********
	* set quaternion given polar
	*
	*********/
	withinRange();
	quat[0] = cos(theta*rad/2);
	quat[1] = sin(theta*rad/2)*cos(lat*rad)*cos(lon*rad);
	quat[2] = sin(theta*rad/2)*cos(lat*rad)*sin(lon*rad);
	quat[3] = sin(theta*rad/2)*sin(lat*rad);
}


/**********************************************************************/
/*************************** FRIENDS  *********************************/
/**********************************************************************/

// Output method

ostream & operator<<(ostream & os, const Rotation & r) {
	if (r.getplate() < 10) os << "00" << r.getplate();
	else if (r.getplate() < 100) os << "0" << r.getplate();
	else os <<  r.getplate();
	os.setf(ios::showpoint);
	os.setf(ios::fixed,ios::floatfield);
	//os.setf(ios::internal,ios::adjustfield);
	os.precision(2);
	os << " " << r.gettime() << " ";
	os.precision(4);
	os << r.getlat() << " ";
	os << r.getlon() << " ";
	os.precision(6);
	os << r.getang() << " ";
	os.unsetf(ios::showpoint);
	if (r.getcplate() < 10) os << " 00" << r.getcplate();
	else if (r.getcplate() < 100) os << "  0" << r.getcplate();
	else os << " " <<  r.getcplate();
	os << endl;
	return os;
}

