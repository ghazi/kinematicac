/*
Getrot - A program for calculating and interpolating rotations
part of the Kinematica suite
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Getrot-main.cpp is part of the Getrot program

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Getrot-main.cpp - This file
 *	The main control program for getrot
 *      Version 2.0 April 24th, 2013
 *
 **/


#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#define CODEVERSION 2.0
#define REVISIONDATE "24th April, 2013"
#include "Getrot.h"


void usage(char *n);

int main (int argc, char * argv[]) {

      
	double time = 0.0;
        int plate1 = 000;
	int plate2 = 000;
        string rotfile = "global2000.rot";

if (argc < 2) {
       	usage(argv[0]); 
	return 1;
}

if ((argv[1][0] == '-') && ((argv[1][1] == 'v') || (argv[1][1] == 'V'))){

	 cerr << argv[0] << " version " << CODEVERSION << " " << REVISIONDATE << "\n";
             return 0;
}

if ((argv[1][0] == '-') && ((argv[1][1] == 'h') || (argv[1][1] == 'H'))){
		usage(argv[0]);
	       	return 0;
}

if (argc < 5) {
       	cerr << "Minimum of four options required\n";
       	usage(argv[0]); 
	return 1;
}
int i = 1;

plate1 = atoi(argv[1]);
plate2 = atoi(argv[2]);

if ((plate1 == 999) || (plate2 == 999)) {
	usage(argv[0]);
	return 1;
}

i = 3;
while (i < argc) {
       	if (argv[i][0] != '-') {
	       	cerr << "Error using option " << argv[i] << endl; 
		usage(argv[0]); 
		return 1;
       	}
                        switch (argv[i][1]) {

                                        case 't':
                                        case 'T':       i++; 
							time = atof(argv[i]);
							break;
					case 'r':
                                        case 'R':       i++;
                                                        rotfile = argv[i];
                                                        break;

                                        default:
                                                         cerr << "Invalid option " << argv[i] << endl;
                                                        usage(argv[0]);
                                                        return 1;

                        } // switch
	i++;
        } // while

        Getrot program(rotfile.c_str(),time,plate2,plate1);
        program.run();
        return 0;
} // main


void usage (char *n) {

        /*********
         ** usage() to show all options
         **
         ******/

cerr << n << " v2.0 April 24, 2013\n\n";
cerr << "usage:     " << n << " n1 n2 -t t1 -r rot\n\n\n";
cerr << n << "[-hHvV]\n\n\n";
cerr << "arguments:\n";
cerr << "n1:  plate id of the moving plate\n";
cerr << "n2:  plate id of the fixed plate\n";
cerr << "t1:  age of the reconstruction (in Myr)\n";
cerr << "rot: rotation file to be used\n\n";
cerr << "Output is: latitude longitude angle\n\n";
cerr << "options:\n";
cerr << "   -h\n    -H";
cerr << "   - Displays this help\n";
cerr << "   -v\n    -V";
cerr << "          - Display version.\n\n";
cerr << "Author: Stuart Clark\n";
cerr << "Bugs:   dr.stuartclark@gmail.com\n\n";

}
