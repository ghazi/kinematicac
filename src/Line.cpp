/*
The Line class
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Line.cpp is part of the Kinematica suite

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Line.cpp - This file
 *	The main class for handling line data in latitude/longitude
 *      Version 2.0 April 24th, 2013
 *
 **/


#include "Line.h"

enum Linetype { IS = 0 , IM, CO, CM , RI, XR };
				/* IS : Isochron (Default)
                                   IM : M-Isochron
                                   CO : Continental-Oceanic Boundary
                                   RI : Mid-Ocean Ridge/Plate Boundary
                                   XR : Extinct Mid-Ocean Ridge
                                */


/**********************************************************************/
/***************** CONSTRUCTORS/DESTRUCTORS ***************************/
/**********************************************************************/

Line::Line() {


/********
	* The Default Constructor
	*
	*****/
	
	region = 00;
	linenum = 1;
	plate = 002;
	cplate = 000;
	time = 0.0;
	dtime = -999.0;
	type = IS;
	colour = 1;
	number = 0;
}

Line::Line(int r,int lnum,int id1,int id2,double t,double dt,int typ,int col,int n,bool GMT) {

/********
	* The Constructor to incorporate all information
	* from the plates format
	*
	********/

	region = r;
	linenum = lnum;
	plate = id1;
	cplate = id2;
	time = t;
	dtime = dt;
	switch (typ) {

			case 1 : type = CO;
				 break;
			case 2 : type = RI;
				 break;
			case 3 : type = XR;
				 break;
			case 4 : type = IM;
				 break;
			default : type = IS;
	}
			
	colour = col;
	number = n;
	fisGMToutput = GMT;
}



Line::~Line() {

}


/**********************************************************************/
/***************** PRIVATE MEMBER FUNCTIONS ***************************/
/**********************************************************************/

void Line::check() {

/********
	* checks that the number in the header
	* is the same as the number of points
	*
	******/

	int num = 0;
	for (list <Point>::iterator check = data.begin();
		check != data.end();
		check++) {
			num++;
	}
	if (num != number) {

		number = num;
	}
return;
}

/**********************************************************************/
/***************** PUBLIC MEMBER FUNCTIONS ****************************/
/**********************************************************************/


void Line::insert(Point & p) {

/********
	* Inserts a point at the end
	*
	*****/

// Note insertion does not update number -- this is fixed in the header
	
	p.setXY(fisGMToutput);
	data.insert(data.end(),p);
	
}

void Line::rotate(Rotation &r) {

/********
	* Rotates the entire line
	* 
	*****/

list<Point> tmp;

for (list<Point>::iterator rot = data.begin();
		 rot != data.end();
		 ++rot) {
	Point temp = *rot;
	Point temp2 = r * temp;
	tmp.insert(tmp.end(),temp2);
	

}
	
data = tmp;

}

list <Line> Line::interpolate(Line &l,Rotation &r, double *mindist, double *maxdist, double *avgdist) {

/********
	* This Method Interpolates between two lines. 
	* It returns a list of the interpolated lines 
	* including age, spreading velocity,
	* spreading direction and spreading asymmetry for every point (contained within the Point object).
	* Additional information about the min/max/average distance between the lines is reported by the variables mindist,maxdist,avgdist
 	*******/



// Rotate Lines to Stage Pole reference frame 
// i.e. rotate stage pole to North Pole

Rotation NP = r.getNP();
Rotation back = -NP;
double theta = r.getang();
l.rotate(NP);
rotate(NP);

list <Point> young,old;
Line a,b;

// young is the data that goes with the youngest line
// a is the corresponding header (uses class Line with empty data)
// old is the data for the oldest line
// b is the corresponding header (uses class Line with empty data)

if (l.gettime() < gettime()) {

	young = l.data;
	a = l;
	old = data;
	b = *this;
}

else {

	young = data;
	a = *this;
	old = l.data;
	b = l;
	
}
// Check number of points in data

a.check();
b.check();

// Retain headers of data only;
a.data.clear();
b.data.clear();

// Make sure that Lines are NS oriented and not SN

if (young.front().getlat() < young.back().getlat()) {
	list <Point> tmp;
	for (list <Point>::iterator sorter = young.begin();
		sorter != young.end();
		++sorter) {
		tmp.insert(tmp.begin(),*sorter);

	}
	young = tmp;
}
	
if (old.front().getlat() < old.back().getlat()) {
	list <Point> tmp;
	for (list <Point>::iterator sorter = old.begin();
		sorter != old.end();
		++sorter) {
		tmp.insert(tmp.begin(),*sorter);

	}
	old = tmp;
}

// Correct for Reverse Offsets

list <Point> off;
off.insert(off.end(),young.front());
double inc = 0.0000001;
list <Point>::iterator beforeoffset = young.begin();
for (list <Point>::iterator offset = ++young.begin();
	offset != young.end();
	++offset) {
		Point before = *beforeoffset;
		Point current = *offset;

		if (current.getlat() > before.getlat()) {

			Point xy(current.getlon(),(before.getlat() - inc));
			off.insert(off.end(),xy);
			inc += 0.0000001;
		}

		else {
			off.insert(off.end(),current);
			beforeoffset = offset;
			inc = 0.0000001;
			
		}
}
young = off;
off.clear();			
off.insert(off.end(),old.front());
inc = 0.0000001;
beforeoffset = old.begin();
for (list <Point>::iterator offset = ++old.begin();
	offset != old.end();
	++offset) {
		Point before = *beforeoffset;
		Point current = *offset;

		if (current.getlat() > before.getlat()) {

			Point xy(current.getlon(),before.getlat());
			off.insert(off.end(),xy);
			inc += 0.0000001;
		}

		else {
			off.insert(off.end(),current);
			beforeoffset = offset;
			inc = 0.0000001;
		}
}
old = off;


// Ensure that latitudinal difference (in NP reference frame)
// between two adjacent segments not too large in the 
// case of a CM - IS interpolation
// but is large for a IS - IS interpolation

double latdif;
if (b.type == CO) {latdif = 3;} 	// CO/IS IM/CO difference allowed
else {latdif = 3;}			// IS/IS and IM/IM difference allowed

// Do not include points beyond the latitude cutoff

list <Point> ysize,osize;
double ymax = young.front().getlat();
double ymin = young.back().getlat();
double omax = old.front().getlat();
double omin = old.back().getlat();

for (list <Point>::iterator sizer = young.begin();
	sizer != young.end();
	++sizer) {

	Point current = *sizer;
	double lat = current.getlat();
	if ((lat > (omax + latdif))||(lat < (omin - latdif))) ;
	else ysize.insert(ysize.end(),current);

}


for (list <Point>::iterator sizer = old.begin();
	sizer != old.end();
	++sizer) {

	Point current = *sizer;
	double lat = current.getlat();
	if ((lat > (ymax + latdif))||(lat < (ymin - latdif))) ;
	else osize.insert(osize.end(),current);


}

young = ysize;
old = osize;

// Check that the Data does not pass the 0/360 Boundary and shift if necessary

double minyoung,maxyoung,minold,maxold;
double shift = 0.0;

minyoung = 360.0;
minold = 360.0;
maxyoung = -360.0;
maxold = -360.0;

for (list <Point>::iterator shifter  = young.begin();
	shifter != young.end();
	++shifter) {

	Point current = *shifter;

	if (current.getlon() < minyoung) minyoung = current.getlon();
	if (current.getlon() > maxyoung) maxyoung = current.getlon();

}
for (list <Point>::iterator shifter  = old.begin();
	shifter != old.end();
	++shifter) {

	Point current = *shifter;

	if (current.getlon() < minold) minold = current.getlon();
	if (current.getlon() > maxold) maxold = current.getlon();

}

if (((maxyoung - minyoung) > 180) || ((maxold - minold) > 180)) {

list <Point> tmp1,tmp2;

for (list <Point>::iterator shifter  = young.begin();
	shifter != young.end();
	++shifter) {

	Point current = *shifter;
	if (current.getlon() > 180) current.shift();
	tmp1.insert(tmp1.end(),current);
	
} // for shifter loop

for (list <Point>::iterator shifter  = old.begin();
	shifter != old.end();
	++shifter) {

	Point current = *shifter;
	if (current.getlon() > 180) current.shift();
	tmp2.insert(tmp2.end(),current);


} // for shifter loop

young = tmp1;
old = tmp2;

} // if needs shifting

// Ensure that the Lines contain points at the same latitude
// Using Linear interpolation

list <Point> everylat = young;
list <Point> temp = old;
everylat.merge(temp);


for (list <Point>::iterator pointe = everylat.begin();
	pointe != everylat.end();
	++pointe) {

	Point current = *pointe;
	double alon=0,alat=0,blon=0,blat=0;
	double elon = current.getlon();
	double elat = current.getlat();
	
	if (a.number == 1) {
		cerr << "Only one point in young line!\n";
		alon = elon - theta/2;
		alat = elat;
	}
		
	else if (elat > young.front().getlat()) {
		alon = young.front().getlon();
		alat = young.front().getlat();
				
	}

	else if (elat < young.back().getlat()) {
		alon = young.back().getlon();
		alat = young.back().getlat();
	} 

	else { // Find the right pair to do Linear interpolation with
		Point before;
		for (list <Point>::iterator pointy = young.begin();
			pointy != young.end();
			++pointy) {
			
			before = *pointy;
			
			if (elat == before.getlat()) {
				alon = elon;
				alat = elat;
				break;
			}
			else if (elat < before.getlat()) {
				// before != young.back
				list <Point>::iterator pointy2 = pointy;
				pointy2++;
				Point after = *pointy2;
				if (elat > after.getlat()) {
					// Linear N/S Interpolation 
					double y1 = before.getlat();
					double x1 = before.getlon();
					double y2 = after.getlat();
					double x2 = after.getlon();
					double delta = (elat - y1)/(y2 - y1);
					alon = x1 + delta * (x2 - x1);
					alat = elat;
					break;
				}

				else {
					// We are not far enough along the line yet
					continue;
				}
	
			}

			else {  // (elat > before.getlat()
				// since elat < young.begin().getlat() but this
				// implies elat is bigger than all previous points
				// and therefore bigger than the beginning
				// which has already been dealt with
				cerr << "Error in N/S interpolation of data\n";	
			}
		} // for loop
	}

	Point interpy(alon,alat);
	a.insert(interpy);

	if (b.number == 1) {

		cerr << "Only one point in old line!\n";
		blon = elon - theta/2;
		blat = elat;
	}
		
	else if (elat > old.front().getlat()) {
		blon = old.front().getlon();
		blat = old.front().getlat();
				
	}

	else if (elat < old.back().getlat()) {
		blon = old.back().getlon();
		blat = old.back().getlat();
	} 

	else { // Find the right pair to do Linear interpolation with
		Point before,after;
		for (list <Point>::iterator pointo = old.begin();
			pointo != old.end();
			++pointo) {
			
			before = *pointo;
			
			if (elat == before.getlat()) {
				blon = elon;
				blat = elat;
				break;
			}
			else if (elat < before.getlat()) {
				
				// before != young.back
				list <Point>::iterator pointo2 = pointo;
				pointo2++;
				Point after = *pointo2;
				
				if (elat > after.getlat()) {
				
					// Linear N/S Interpolation 
					double y1 = before.getlat();
					double x1 = before.getlon();
					double y2 = after.getlat();
					double x2 = after.getlon();
					double delta = (elat - y1)/(y2 - y1);
					blon = x1 + delta * (x2 - x1);
					blat = elat;
					break;
				}

				else {
					// We are not far enough along the line yet
					continue;
				}
	
			}

			else {  // (elat > before.getlat()
				// since elat < old.begin().getlat() but this
				// implies elat is bigger than all previous points
				// and therefore bigger than the beginning
				// which has already been dealt with
				cerr << "Error in N/S interpolation of data\n";	
			}
		} // for loop
	}

	Point interpo(blon,blat);
	b.insert(interpo);
} // for loop

// Check that the Data does not pass the 0/360 Boundary and shift if necessary

shift = 0.0;

minyoung = 360.0;
minold = 360.0;
maxyoung = -360.0;
maxold = -360.0;

for (list <Point>::iterator shifter  = a.data.begin();
	shifter != a.data.end();
	++shifter) {

	Point current = *shifter;

	if (current.getlon() < minyoung) minyoung = current.getlon();
	if (current.getlon() > maxyoung) maxyoung = current.getlon();

}
for (list <Point>::iterator shifter  = b.data.begin();
	shifter != b.data.end();
	++shifter) {

	Point current = *shifter;

	if (current.getlon() < minold) minold = current.getlon();
	if (current.getlon() > maxold) maxold = current.getlon();

}

if (((maxyoung - minyoung) > 180) || ((maxold - minold) > 180)) {

list <Point> tmp1,tmp2;

for (list <Point>::iterator shifter  = a.data.begin();
	shifter != a.data.end();
	++shifter) {

	Point current = *shifter;
	if (current.getlon() > 180) current.shift();
	tmp1.insert(tmp1.end(),current);
	
} // for shifter loop

for (list <Point>::iterator shifter  = b.data.begin();
	shifter != b.data.end();
	++shifter) {

	Point current = *shifter;
	if (current.getlon() > 180) current.shift();
	tmp2.insert(tmp2.end(),current);


} // for shifter loop

a.data = tmp1;
b.data = tmp2;

} // if needs shifting

	

// Overlap Check

Line tempa = a;
Line tempb = b;
tempa.data.clear();
tempb.data.clear();

int igt = 0;
int ilt = 0;
list <Point>::iterator overlapb = b.data.begin();
for (list <Point>::iterator overlapa = a.data.begin();
	overlapa != a.data.end();
	++overlapa) {
	
	Point acurrent = *overlapa;
	Point bcurrent = *overlapb;

	if (acurrent.getlon() < bcurrent.getlon()) ilt += 1;
	else igt += 1;

++overlapb;
}

if (ilt > igt) {
	list <Point>::iterator overlapb = b.data.begin();
	for (list <Point>::iterator overlapa = a.data.begin();
		overlapa != a.data.end();
		++overlapa) {

		Point acurrent = *overlapa;
		Point bcurrent = *overlapb;

		if (acurrent.getlon() > bcurrent.getlon()) {
			if (a.gettype() != 1) {
				tempa.insert(acurrent);
				tempb.insert(acurrent);
			}
			else {
				tempa.insert(bcurrent);
				tempb.insert(bcurrent);
			}

		}
		else {
			tempa.insert(acurrent);
			tempb.insert(bcurrent);
		}
	++overlapb;
		
	}

}

else {
	list <Point>::iterator overlapb = b.data.begin();
	for (list <Point>::iterator overlapa = a.data.begin();
		overlapa != a.data.end();
		++overlapa) {
			Point acurrent = *overlapa;
			Point bcurrent = *overlapb;
		if (acurrent.getlon() < bcurrent.getlon()) {
			if (a.gettype() != 1) {
				tempa.insert(acurrent);
				tempb.insert(acurrent);
			}
			else {
				tempa.insert(bcurrent);
				tempb.insert(bcurrent);
			}

		}
		else {
				tempa.insert(acurrent);
				tempb.insert(bcurrent);
		}
	++overlapb;
	
	}


}

a = tempa;
b = tempb;

// Calculate Number of Interpolations
double totdist=0.0;
*avgdist = 0.0;
*maxdist = 0.0;
*mindist = -1.0;
int totnum = 0;

list<Point>::iterator ait = a.data.begin();
list<Point>::iterator bit = b.data.begin();
	while (ait != a.data.end()) {
		totnum++;
		Point acurrent = *ait;
		Point bcurrent = *bit;
		double distance = acurrent.dist(bcurrent);
		if (distance > *maxdist) 
				*maxdist = distance;
		if ((distance < *mindist)|| (*mindist < 0.0))
				*mindist = distance;
		if (distance > 1800) {
			cerr << "Some points between the two lines below were greater than 30 degrees apart:" << a << b;
			std::list<Line> empty;
			return empty;
		}
		else 
			totdist += distance;
		++ait;
		++bit;
	}
if (totnum > 0) 
	*avgdist = totdist/totnum;
else
	*avgdist = 0.0;
int avg = (int)*avgdist;
int num = ((int)avg)/2 +1;
if (*maxdist > 2 * avg) 
	num *=2;
if (num < 3)
	num = 3;

double agediff = b.time - a.time;
if (agediff > num) num = (int) agediff;
if (*avgdist < 60.0) num =10;

// Set a Maximum (for debugging only)

// MAXIMUM 
if (num > 40) num = 40;

// Now interpolate
list <Line> output;
// Leaves out oldest isochron unless it is a COB segment
if (b.type == CO) num +=1;
for (int i = 0; i<num;i++) {
	double bdelta = i/((double)num);
	double adelta = 1 - bdelta;
	ait = a.data.begin();
	bit = b.data.begin();
	time = adelta * a.gettime() + bdelta * b.gettime();
	int typ = 0;
	if (i == 0) typ = (int) a.type;
	if ((b.type == CO) && (i == num-1)) typ = (int)b.type;
	Line actual = Line(a.region,a.linenum,a.plate,a.cplate,time,dtime,typ,a.colour,a.number+b.number,a.fisGMToutput);
	while (ait != a.data.end()) {

		Point acurrent = *ait;
		Point bcurrent = *bit;
		double lon,lat,llon,llat,ltime,rlon,rlat,rtime;
		double dlon = acurrent.getlon() - bcurrent.getlon();
		double dlat = acurrent.getlat() - bcurrent.getlat();
		double dtime = b.gettime() - a.gettime();
		lon = adelta * acurrent.getlon() + bdelta * bcurrent.getlon();
		lat = adelta * acurrent.getlat() + bdelta * bcurrent.getlat();
		double n = (double) num;
		llon = lon - 0.5 * (1/n) *dlon;
		llat = lat - 0.5 * (1/n) * dlat;
		ltime = time - 0.5 *(1/n)*dtime;
		rlon = lon + 0.5 * (1/n) *dlon;
		rlat = lat + 0.5 * (1/n) * dlat;
		rtime = time + 0.5 *(1/n)*dtime;
		Point left(llon,llat);
		Point right(rlon,rlat);
		Point nleft = left*back;
		Point nright = right*back;
		double course = nleft.cour(nright);
		double distance = nleft.dist(nright);
		if (course < -180) course += 360;
		if (course > 180) course -= 360;
		double dage,vel,asym;
		dage = rtime - ltime;
		asym = 50.0;
		if (dage != 0) {
			vel = distance/dage; // km/Myr
			vel /= 10; // cm/yr
		}
		else vel = 0.0;
		if (theta != 0) asym = abs((dlon*100)/theta);
		if (asym > 100) asym = 100;
		if (asym <= 0) asym = 0.1;
		if ((vel >150) && (vel < 250)) vel = 150;
		if (vel > 250) vel = -999;
		if (vel <0) vel = 0.01;
		Point mid(lon,lat,time,vel,course,asym);
		actual.insert(mid);
		++ait;
		++bit;
	}
	actual.rotate(back);
	actual.check();
	output.insert(output.end(),actual);

} // for loop
return output;
} // interpolate()

bool Line::operator==(Line &m) {

/********
	* Two Lines are the same if plate/c. plate and age are the same
	*
	******/

	if ((plate == m.plate) && (cplate == m.cplate) && (time == m.time)) return true;

return false;

}

bool Line::operator!=(Line &m) {

return (!((*this)==m));

}

bool Line::operator<(Line &m) {


// Organise lines firstly by Plate ID
if (plate < m.plate) return true;
if (plate > m.plate) return false;
// Then by conjugate Plate ID
if (cplate < m.cplate) return true;
if (cplate > m.cplate) return false;
// Then by type
if ((type == RI) && (m.type == CO)) return true;
if ((type == XR) && (m.type == CO)) return true;
if ((type == CO) && (m.type == RI)) return false;
if ((type == CO) && (m.type == XR)) return false;
if ((type == IS) && (m.type == CO)) return true;
if ((type == IM) && (m.type == CO)) return true;
if ((type == CO) && (m.type == IS)) return false;
if ((type == CO) && (m.type == IM)) return false;
// Then by time
if (time < m.time) return true;
if (time > m.time) return false;
// And finally by position
if (data.front().getlat() > m.data.front().getlat()) return true;

return false;

}

/***************************************************************/
/********************** FRIENDS ********************************/
/***************************************************************/
		
list <Line> concatenate(list<Line> &conc,Rotation &stage) {

// Rotate to Stage Pole RF
	Rotation NP = stage.getNP();
	Rotation back = -NP;


for (list<Line>::iterator rotate=conc.begin();
	rotate != conc.end();
	rotate++) {
			(*rotate).rotate(NP);
}

// Make Sure lines are NS oriented and not SN

list <Line> tmp;
for (list<Line>::iterator NS =conc.begin();
	NS != conc.end();
	NS++) {

	Line current = *NS;
	if (current.data.front().getlat() < current.data.back().getlat()) {
		list <Point> temp;
		for (list<Point>::iterator sorter = current.data.begin();
			sorter != current.data.end();
			sorter++) {
				temp.insert(temp.begin(),*sorter);
			}
			current.data = temp;
		}
		tmp.insert(tmp.end(),current);
	}
	conc = tmp;
	// Sort list
	conc.sort();


list<Line> output;
list<Line>::iterator check1 = conc.begin();
Line before = *check1;

	for (list<Line>::iterator check2 = ++conc.begin();
		check2 != conc.end();
		++check2) {

		Line after = *check2;

		if (before.data.back().getlat() < after.data.front().getlat() + 1.0) {
			for (list<Point>::iterator merger = after.data.begin();
				merger != after.data.end();
				++merger) {
				before.data.insert(before.data.end(),*merger);
			}

		}

		else {

			output.insert(output.end(),before);
			check1 = check2;
			before = *check1;	

		}

	}

output.insert(output.end(),before);


for (list<Line>::iterator rotate=output.begin();
	rotate != output.end();
	rotate++) {
			(*rotate).check();
			(*rotate).rotate(back);
}
return output;
} // concatenate

ostream & operator<<(ostream & os,const Line & l) {

/********
	* Output in two formats Plates and GMT
	*
	******/

	// Don't output at all if this is an empty line       
        if (l.data.empty ())                                  
                return os;  

	if (!l.fisGMToutput) {

	if (l.region < 100) os << l.region << " 1 ";
	else os << l.region << " ";	
	os.width(4);
	os << l.linenum;
	os.width(4);
	os << l.linenum << " Kinematica Line\n";
	os.setf(ios::fixed,ios::floatfield);
	if (l.plate < 10) os << "  00" << l.plate;
        else if (l.plate < 100) os << "   0" << l.plate;
        else os << " " << l.plate << " ";
	os.setf(ios::showpoint);
	os.setf(ios::right,ios::adjustfield);
	os.width(6);
	os.precision(1);
	os <<  l.time << " -999.0";
	switch (l.type) {
		case CO : os << " CO   0 ";
                          break;
		case RI : os << " RI   0 ";
                          break;
		case XR : os << " XR   0 ";
                          break;
		case IM : os << " IM   0 ";
                          break;
		default : os << " IS   0 ";
                          break;
	}
	os.setf(ios::right,ios::adjustfield);
	os.width(3);
	if (l.cplate < 10) os << " 00" << l.cplate;
        else if (l.cplate < 100) os << "  0" << l.cplate;
        else os << l.cplate;
	os.width(4);
	os << l.colour;
	os.width(6);
	os  << l.number; 
	os << endl;
	ostream_iterator<Point> out3(os," 3\n");
	copy(l.data.begin(),++l.data.begin(),out3);
	ostream_iterator<Point> out2(os," 2\n");
	copy(++l.data.begin(),l.data.end(),out2);
	os << "  99.0000   99.0000 3\n";
	return os;
} // !fisGMToutput

else {

	os << ">\n";
	ostream_iterator<Point> out(os,"\n");
	copy(l.data.begin(),l.data.end(),out);
	
}
	return os;
} // << operator

bool check(Line &l,Line &m,Rotation &r) {

/********
	* checks that the COB and the last isochron
	* overlap more than three degrees
	* (m is COB)
	*****/
bool ans = false;

Rotation NP = r.getNP();
Rotation back = -NP;

l.rotate(NP);
m.rotate(NP);

double lfront = l.data.front().getlat();
double lback = l.data.back().getlat();
double mfront = m.data.front().getlat();
double mback = m.data.back().getlat();

// Check if NS or SN oriented

if (lfront < lback) {
			double tmp = lfront;
			lfront = lback;
			lback = tmp;
}

if (mfront < mback) {
			double tmp = mfront;
			mfront = mback;
			mback = tmp;
}


if ((lfront > mback + 3)&&(lback < mfront - 3))  ans = true;

l.rotate(back);
m.rotate(back);
return ans;

}
