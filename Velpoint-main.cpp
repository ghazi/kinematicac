/*
Velpoint - A Program for calculating velocities of points
Velpoint is part of the Kinematica suite
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Velpoint-main.cpp is part of Velpoint
    Velpoint is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Velpoint-main.cpp - This file
 *	The main control program for command line usage
 *      Version 2.0 April 24th, 2013
 *
 **/

#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <string.h>

#define CODEVERSION 2.0
#define REVISIONDATE "24th April, 2013"
#include "Velpoint.h"
using namespace std;

// Message Functions called by main()

void usage(const char * name);
void error(char o);
void version(char * n);

/** 
  * Main function 
  * used to parse command line parameters and call Document class
  **/

int main(int argc, char * argv[]) {

	int c = 1;
	int i = 0;
	float inputvalues [5];
	float time = 0.0;
	float tmp = 0.0;
	bool swaplatlon = false;
	bool verbose = false;
	bool ftime = false;

	if (argc < 6) {
		usage(argv[0]);
		exit(EXIT_FAILURE);
	}
	while (c < argc) {
		if (strncmp(argv[c],"-:",2) ==0) {
			swaplatlon = true;
		}
		if (strncmp(argv[c],"-v",2) == 0) {
			verbose = true;
		} 
		else if(strncmp(argv[c],"-t",2) == 0 ) {
			if (c+1 < argc) {
				time = atof(argv[c+1]);
				ftime = true;
				c++;
			}
			else {
				usage(argv[0]);
				exit(EXIT_FAILURE);
			}
		}
		else {
			if (i <= 4) {
				inputvalues[i] = atof(argv[c]);
				i++;
			}
			else {
				// Too many arguments
				usage(argv[0]);
				exit(EXIT_FAILURE);
			}
		}
		c++;
	}
	if (swaplatlon) {
		tmp = inputvalues[0];
		inputvalues[0] = inputvalues[1];
		inputvalues[1] = tmp;
		tmp = inputvalues[2];
		inputvalues[2] = inputvalues[3];
		inputvalues[3] = tmp;
	}
	Point p = Point(inputvalues[1],inputvalues[0]); // Point uses (lon,lat) format
	Rotation r = Rotation(inputvalues[3],inputvalues[2],inputvalues[4]); // Rotation uses (lon,lat,ang) format
	Velpoint vel(p,r, time,ftime,verbose);
	vel.run();

} // main()

void error(char o) {

/********
	* Error function - used to output error message
	* for incorrect options usage
	*
	*****/

	cerr << "Error using option -" << o << endl;
}		

void usage(const char * name) {
	
/********
	* usage() to show all options
	*
	*****/

	cerr << name << "\n version " << CODEVERSION << " " << REVISIONDATE << "\n";
	//cerr << name << "\n v" << "\n";
	cerr << "Usage: " << name << " lat lon rlat rlon rang [-t time]  [-:]\n";
	cerr << "Calculates the distance in km [or velocity in cm/yr if -t time is given] of motion from an original point (lon,lat) using an Euler rotation (rlon, rlat, rang). If velocity is desired, then [-t time] is required to set the length of time that the motion occurs over\n";
cerr <<"-:   : swaps longitude and latitude in the input arguments\n";
cerr <<"        for both the point and the rotation pole\n";
cerr <<"-v for verbose output\n";
cerr <<"-h for this help\n";
cerr << "Author: Stuart Clark\n";
cerr << "Bugs:   dr.stuartclark@gmail.com\n\n";


}

void version(char * n) {

/********
	* show version number
	*
	*****/

	cerr << n << " v2.0 24th April, 2013\n\n";


}
