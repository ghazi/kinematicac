/*
The Rotation class
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Rotation.h is part of the Kinematica suite

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Rotation.h - This file
 *	The main class for handling spherical rotations in Euler pole/angle or as quaternions. 
 *      Version 2.0 April 24th, 2013
 *
 **/

#ifndef _ROTATION_H_
#define _ROTATION_H_

#include <iostream>
#include <iterator>
#include <cmath>
using namespace std;

#include "Point.h"

class Rotation : public Point{

	private:
		int plate;
		int cplate;
		double time;
		double theta;
		double quat[4];

		bool fisstage;
		double timeend; 

		void setpolar();
		void setquat();


	public:
		// Constructors

			Rotation();	// default contructor
			Rotation(double X,double Y,double ang,int p=0,int cp=0,double t=0.0);
			Rotation(double * q,int p,int cp,double t);

			Rotation(double X,double Y,double ang,int p,int cp,double t,double te);

		// Destructor
		
			~Rotation();
		
		// Methods
			void part(const double d,const double t);
			Rotation * slerp(const Rotation r,const double t);
			void settime(const double t) {time =t;};
			void setang(const double t) {theta =t;setquat();};
			void swap();
			void toNP();
			void withinRange();
			Rotation getNP(); 
			Rotation operator+(Rotation & r);
			Rotation operator-();
			Rotation operator-(Rotation & r);
			Point operator*(Point &p);
			bool operator==(Rotation &r);
			bool operator<(Rotation &r);
		// Print Members	
			int getplate() const { return plate; };
			int getcplate() const { return cplate; };
			double getang() const { return theta; };
			double gettime() const { return time; };
			bool isstage() const { return fisstage; };
			double gettimeend () const{ if (fisstage) return timeend; return 0.0; };
		// Friends
			friend ostream & operator<<(ostream & os, const Rotation & r);
			friend Point operator*(Point &p,Rotation &r);
	
};
#endif
