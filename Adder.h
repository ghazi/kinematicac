/*
Adder - A Program for calculating velocities of points
Adder is part of the Kinematica suite
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Adder.h is part of Adder
    Adder is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Adder.h - This file
 *	The main class for handling velocity and distance calculations
 *      Version 2.0 April 24th, 2013
 *	Original version 1.0 ready January 30th, 2004
 *
 **/

#ifndef _ADDER_H_
#define _ADDER_H_

#include <fstream>
#include <list>

#include <Point.h>
#include <Rotation.h>

class Adder {

	private:

		// Reconstruction time and frame
		Rotation rotationA;
		Rotation rotationB;
		bool fverbose;

	public: 
		// Constructor/Destructor

		Adder(Rotation r1, Rotation r2, bool verb=false);

		// Main control program

		int run();
		
};
#endif
