/*
Intertec - A Program for reconstructing and interpolating isochrons, plateboundaries and continent-ocean boundaries.
Intertec is part of the Kinematica suite
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Intertec.cpp is part of the Intertec program

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Intertec-main.cpp - This file
 *	The main class for the Intertec program
 *      Version 2.0 April 24th, 2013

 Intertec
 Version 2.0 24 April 2013
 Version 1.21 20 November 2012
 Version 1.2 4 April 2002
 Version 1.0 15 December 1999
 Copyright Stuart Clark
 Minor Changes between 1.2 and 1.21 by James Boyden
**/

#include <iostream>
#include <cmath>
#include <stdlib.h>
#define CODEVERSION 2.0
#define REVISIONDATE "24th April, 2013"
#include "Intertec.h"
using namespace std;

// Message Functions called by main()

void usage(const char * n);
void option_error(const char o);
int file_check(const int nfile, const char * file);
void version(const char * n);

/** 
  * Reference Frame Structure
  * Including reference frame time
  * and reference frame plateid
  **/

struct Frame {
	double time;
	int plate;
	string outputfile;
};

/** 
  * Main function 
  * used to parse command line parameters and call Document class
  **/

int main(int argc, char * argv[]) {

	bool GMT = true;
	bool debug = false;
	int plateid = 895;
	int cplateid = 801;
	bool duplicates = true;
	// inputfiles
	string inputfile[4];

	bool interpolate = true;

	// Initialize list of Frame sets (time,plate,outputfile)

	list <Frame> framelist;

	int ones,tens,hundreds,num,num2,j;
	double sum = 0.0;
	Frame frame;

for (int i=1 ; i < argc ; i++) {

	switch(argv[i][1]) {
			case 'p':
			case 'P': GMT = false;
				  break;
			case 'v':
			case 'V': version(argv[0]);
				  return 0;
			case 'd':
			case 'D': debug = true;
				  i++;
				  if (argv[i][3] != '\0') {
				  	option_error(argv[i--][1]);
				  	usage(argv[0]);
				  	exit(1);
				  }
				  if ((argv[i][0] < '0')&&(argv[i][0] > '9')) {
				  	option_error(argv[i--][1]);
				  	usage(argv[0]);
				  	exit(1);
				  }
				  hundreds = (int)argv[i][0] - (int)'0';
				  tens = (int)argv[i][1] - (int)'0';
				  ones = (int)argv[i][2] - (int)'0';
				  plateid = 100 * hundreds;
				  plateid += 10 * tens + ones;
				  i++;
				  if (argv[i][3] != '\0') {
				  	option_error(argv[i][0]);
				  	usage(argv[0]);
				  }
				  hundreds = (int)argv[i][0] - (int)'0';
				  tens = (int)argv[i][1] - (int)'0';
				  ones = (int)argv[i][2] - (int)'0';
				  cplateid = 100 * hundreds;
				  cplateid += 10 * tens + ones;
				  break;
			case 'f':
			case 'F': i++;	
				  if (argv[i][3] != '\0') {
				  	option_error(argv[i][1]);
				  	usage(argv[0]);
				  }
				  hundreds = (int)argv[i][0] - (int)'0';
				  tens = (int)argv[i][1] - (int)'0';
				  ones = (int)argv[i][2] - (int)'0';
				  frame.plate = 100 * hundreds;
				  frame.plate += 10 * tens + ones;
				  i++;
				  j = 0;
				  while ((argv[i][j] != '.')&&(argv[i][j] != '\0')) {
					j++;
				  }
				  num = 0;
				  num2 = 0;
				  
				  num = j;
				  if (argv[i][j] != '\0') {
					j++;
					while (argv[i][j] != '\0') {
					j++;
					}
				  
				  num2 = j - num -1;
				  }
				  sum = 0.0;
				  for (j = 0; j < num ;j++) {
					sum += (double)(((int)argv[i][j] - (int)'0')* pow(10.0,num - j -1));
				  }
				  for (j = num +1;j < num+num2+1;j++) {
					sum += (double)(((int)argv[i][j] -(int)'0')* pow(10.0,num-j));
				  }
				  frame.time = sum;
				  // Now parse for filename
				  i++;	
				  frame.outputfile = argv[i];
				  framelist.push_back(frame);
				  break;
			case 'h' :
			case 'H' : usage(argv[0]);
				   return 0;
			case 'i':
			case 'I':
				   i++;	
				   inputfile[0] = argv[i];
				   break; 
			case 'c':
			case 'C':
				   i++;	
				   inputfile[1] = argv[i];
				   break; 
			case 'b':
			case 'B':
				   i++;	
				   inputfile[2] = argv[i];
				   break; 
			case 'r':
			case 'R':
				   i++;	
				   inputfile[3] = argv[i];
				   break;
			case 'o':
			case 'O':
				   i++;	
				   inputfile[4] = argv[i];
				   break;
			case 's':
			case 'S':
				   // Suppress Duplicates
				   duplicates = false;
				   break;
			case 't':
			case 'T':
				  interpolate = false;
				  break;
			default:
				   option_error(argv[i][1]);
				   usage(argv[0]);
				   exit(1);
	} // switch
	

} // for loop

// If no Frame details have been provided supply default ones
if (framelist.empty()) {
	Frame default_frame;
	default_frame.time = 0.0;
	default_frame.plate = 000;
	default_frame.outputfile = "out.dat";
}

	// Check for file existence
bool checker = true;
for (int file_i = 0; file_i < 5; ++file_i) {
 	checker = (checker and !file_check(file_i,inputfile[file_i].c_str()));
	if (!checker) {
		cerr << "Quitting due to error in input file(s)\n";
		exit(1);
	}
}
Document Intertec(inputfile,plateid,cplateid,GMT,debug);
if (duplicates == false) {
	Intertec.suppress_duplicate_ISOs();
}
if (interpolate) {Intertec.interpolate();}
	for (list<Frame>::iterator i = framelist.begin(); i != framelist.end(); ++i) {
			Intertec.reconstruct(i->time,i->plate,i->outputfile);
	}
return 0;

} // main()

int file_check(const int nfile, const char * file) {
        ifstream checker;
	checker.open(file);
        if (checker.bad()) {
                cerr << "Error opening file " << file << " check the file is exists and is readable\n";
		return 1;
        }
	string filetype;
	switch (nfile)  {
		case (0):
			filetype = "Isochron";
			break;
		case (1):
			filetype = "COB";
			break;
		case (2):
			filetype = "Plate Polygon";
			break;
		case (3):
			filetype = "Rotation";
			break;
		case(4):
			filetype = "Stage Rotation output";
			break;
	}
	cout << "Using " << file << " as " << filetype << endl;
	return 0;
}
			


void option_error(const char o) {

/********
	* Error function - used to output error message
	* for incorrect options usage
	*
	*****/

	cerr << "Error using option -" << o << endl;
}		

void usage(const char * n) {
	
/********
	* usage() to show all options
	*
	*****/

	cerr << n << " v1.2 April 4, 2002\n\n";
	cerr << "usage:     " << n << "[options] [&>]\n"; 
	cerr << "options:\n";
	cerr << "   -is <filename>\n    -IS <filename>";
	cerr << "   - Specify the Isochron data file\n";
	cerr << "   -co <filename>\n    -CO <filename>";
	cerr << "   - Specify the Continental/Oceanic Boundary data file\n";
	cerr << "   -bo <filename>\n    -BO <filename>";
	cerr << "   - Specify the Plate Boundary data file\n";
	cerr << "   -r <filename>\n    -R <filename>";
	cerr << "   - Specify the Rotation data file\n";
	cerr << "   -p\n    -P"; 
	cerr << "	   - Output in plates format\n";
	cerr << "   -d\n    -D";
	cerr << "	   - Output only two plate pairs\n";
	cerr << "   -t\n    -T";
	cerr << "   - Do not do any Interpolations\n";
	cerr << "   -d <num1> <num2>\n    -D <num1> <num2>";
	cerr << "	   - Debugging, output only two plate pairs, num1 and num 2\n";
	cerr << "   -f <plateid> <T> <outputfile>\n";
	cerr << "   -F <plateid> <T> <outputfile>\n";
	cerr << "   - Reconstructs data relative  to a fixed plate reference frame plateid\n";
	cerr << "   - at time (expressed in Ma) T and outputs the result to a file outputfile\n";
	cerr << "   -h\n    -H";
	cerr << "   - Displays this help\n";
	cerr << "   -v\n    -V";
	cerr << "	   - Display version.\n\n";
	cerr << "[&>] <filename>:";
	cerr << "	 Redirect standard error to filenamen\n\n";
	cerr << "Author: Stuart Clark\n";
	cerr << "Bugs:   dr.stuartclark@gmail.com\n\n";

}

void version(const char * n) {

/********
	* show version number
	*
	*****/

	cerr << n << " version " << CODEVERSION << " " << REVISIONDATE << "\n";


}
