/*
Intertec - A Program for reconstructing and interpolating isochrons, plateboundaries and continent-ocean boundaries.
Intertec is part of the Kinematica suite
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Intertec.h is part of the Intertec program

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Intertec.h - This file
 *	The main class for the Intertec program
 *      Version 2.0 April 24th, 2013
 *
 **/


#ifndef _INTERTEC_H_
#define _INTERTEC_H_

#include <fstream>
#include <iostream>
#include <list>

#include <Point.h>
#include <Rotation.h>
#include <Line.h>	

class Document {

	private:

		// Data files		
		
		ifstream findata[3];
		ifstream finrot;

		// Reconstruction time and frame

		double ttime;
		int frame;

		// Allow Duplicate Isochrons?

		bool duplicate_isochrons;
		
		// Plate pair to use when debugging
		bool debug;
		int plateid;   
		int cplateid;	

		// Stage Rotation Outfile
		ofstream fout;

		// Data lists  
		list <Line> data;
		list <Rotation> rot;

		// Internal Methods
		list <Rotation> giventime(double & time);
		Rotation getrot(double & time,int & plate, int & cplate);	
		list <Line> interpolate(Line &a,Line &b);
		void join(); // joins lines with same header
	public: 
		// Constructor/Destructor

		Document(string inputfile[5],int pl, int cpl,bool GMT=1,bool db=0); 

		~Document();
		int interpolate();
		// Reconstruct to given frame/time
		int reconstruct(double time=0,int frame=0, string outputfile = string("out.dat"));
		void suppress_duplicate_ISOs(bool suppress=true) {duplicate_isochrons = !suppress;}

		// Main control program

		int run();
		
	

};
#endif
