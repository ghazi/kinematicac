// ----------- Test.cpp ------------------------------------- //
// ----------- Multiple Reconstructions ------------------------------- //

#include <iostream>
#include <list>
#include <iterator>

#include "Document.h"

using namespace std;


int main()
{

	char * n1 = "aus-hemi-iso.dat";
	char * n2 = "swpac_cob.dat";
	char * n3 = "platebound.dat";
	char * n4 = "global-dec01.rot";

	bool GMT = true;
	bool db = true;
	int pl = 895;
	int cpl = 801;

Document Testrun(n1,n2,n3,n4,GMT,db,pl,cpl);

	double time = 85.0;
	int frame = 001;
	char * output = "out-1.2.xy";
Testrun.interpolate();
Testrun.reconstruct(time,frame,output);

return 0;
}
