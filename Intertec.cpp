/*
Intertec - A Program for reconstructing and interpolating isochrons, plateboundaries and continent-ocean boundaries.
Intertec is part of the Kinematica suite
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Intertec.cpp is part of the Intertec program

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Intertec.cpp - This file
 *	The main class for the Intertec program
 *      Version 2.0 April 24th, 2013
 *
 **/


#include "Intertec.h"


/**********************************************************************/
/***************** CONSTRUCTORS/DESTRUCTORS ***************************/
/**********************************************************************/


Document::Document(string inputfile[5],int pl, int cpl, bool GMT,bool db) {

/******** 
	* This Constructor reads in all the data files 
	* including the rotation file and creates the data lists 
	* It also calls the Join() method to join all lines with the same header
	* 
	******/

	plateid = pl;
	cplateid = cpl;
	
	// Duplicate isochrons allowed by default

	duplicate_isochrons = true;
	
	debug = db;

	// Open files
		
	findata[0].open(inputfile[0].c_str());
	findata[1].open(inputfile[1].c_str());
	findata[2].open(inputfile[2].c_str());
	finrot.open(inputfile[3].c_str());
	fout.open(inputfile[4].c_str());
	fout.open("intertec_stage.rot");

	fout << "Stage rotations\n"
		<< "===============\n"
		<< "PlateID time lat     lon   angle ConjPlateID\n"
		<< "--------------------------------------------\n";

	// Read Through Rotation File and Create List
	int p, lineno = 0, errors =0;	
	while (finrot >> p) {
                int cp;
                double lat,lon,ang,time;
                if (p == 999) {
                        finrot.ignore(200,'\n');
                        continue;
                }
                if (finrot >> time >> lat >> lon >> ang >> cp) {
	                Rotation r(lon,lat,ang,p,cp,time);
        	        rot.insert(rot.end(),r);
                	finrot.ignore(200,'\n');
		}
		else {
			++errors;
			std::cerr << "\n\tFailed reading line "	<< lineno << " (with plate ID " << p << "). Skipping it.\n"; 
			finrot.ignore(200,'\n'); 
		} 
        }
	if (errors)
		std::cerr << " <" << errors << " error(s))\n";
	else
		std::cerr << "ok\n";
	lineno=0;
	errors=0;
	// Read through three data files and Create data list
	for (int i =0; i <3 ; i++) {
			
		int reg;
		while (findata[i] >> reg) {
			int lnum,p,cp,num,col,tmp;
			char init,temp;
			double time,dtime;
			bool correct;
			++lineno;
			findata[i] >> lnum;
			findata[i].ignore(200,'\n');
			++lineno;
			if (! (findata[i] >> p >> time >> dtime >> init >> temp >> tmp >> cp >> col >> num)) {
				++errors;
				cerr << "\n\tBad header on line " << lineno << ". Trying anyway.\n";
			}
			Line l(reg,lnum,p,cp,time,dtime,i,col,num,GMT);
			Line m(reg,lnum,cp,p,time,dtime,i,col,num,GMT); 
			correct = ( ((i == 0) && (init == 'I')) ||
				    ((i == 1) && (init == 'C')) ||
				    ((i == 2) && (init == 'R')) ||
				    ((i == 2) && (init == 'X')));
			correct = (p != cp) && (num != 0) && correct;
			if (debug) {
				correct = (((plateid == p) && (cplateid == cp)) || ((plateid == cp) && (cplateid == p))) && correct;
			}
			double lon,lat;
			int n;
			++lineno;
			if (! (findata[i] >> lat >> lon >> n)) {
				++errors;
				cerr << "Bad lat/lon reading on line" << lineno << ". Pushing on\n";
			}
			while (lat != 99.0000) {
				Point p(lon,lat);
				p.setXY(GMT);
				l.insert(p);
				if ((init == 'R')||(init == 'X')) m.insert(p);
				if (!(findata[i] >> lat >> lon >> n)) {
					++errors;
					std::cerr << "Bad lat/long on line "
						<< lineno << ". Pushing on.\n";
				}

			}
			if (correct) {
				data.insert(data.end(),l);
				if ((init == 'R')||(init == 'X')) data.insert(data.end(),m);
			}
		}
		if (errors)
			std::cerr << "  (" << errors << " error(s))\n";
		else
			std::cerr << "ok\n";
	} // for loop

cerr << "Sorting...";
data.sort();
if (!duplicate_isochrons) {
cerr << "ok. Checking for duplicates...";
	// Prune out redundant isochrons
	// (two isochrons/COBs are considered "duplicates" if they have the
	//  same age of appearance and disappearance and same ID pair)
	std::list<Line>::iterator it1, it2;
	for (it1 = data.begin (); it1 != data.end (); ++it1) {
		Line l1 = *it1;
		it2 = it1;
		++it2;
		for (; it2 != data.end (); ++it2) {
			Line l2 = *it2;
			if ((l1.gettime () == l2.gettime ()) &&
			    (l1.getdtime () == l2.getdtime ()) &&
			    (l1.getplate () == l2.getplate ()) &&
			    (l1.getcplate () == l2.getcplate ())) {
				// Duplicates!
				cerr << "\nDiscarding duplicate line with "
					"plate ID pair (" << l2.getplate ()
					<< ", " << l2.getcplate () << ").\n";
				cerr << "Checking...\n";
				data.erase (it2);
				it2 = it1;
				++it2;
				break;
			}
		}
	}
    }

cerr << "ok\n" << "Joining lines...";
join();
cerr << "ok\n";

} // Document()



Document::~Document() {

/********
	* The Destructor closes all file streams 
	*
	****/

	findata[0].close();
	findata[1].close();
	findata[2].close();
	finrot.close();
}

/**********************************************************************/
/**************** PRIVATE MEMBER FUNCTIONS ***************************/
/**********************************************************************/


list <Rotation> Document::giventime(double & time) {

/********
	* This Function reads the Rotation List and Outputs
	* a list of all Rotations for a specific time
	* 
	****/

Rotation current(0.0,90.0,0.0,999,999,0.0);
Rotation previous(0.0,90.0,0.0,999,999,0.0);
list <Rotation> output;
int plate = 999;
for (list<Rotation>::iterator update = rot.begin();
			update != rot.end();
			++update) {
	current = *update;
	if ((current.getplate() == 999) || (current.getplate() == plate)) {
	continue;
	}
	else if (current.gettime() == time) {
			output.insert(output.end(), current);
			plate = current.getplate();
			previous = current;
			
	}

	else if (current.gettime() < time) {
			if ((previous.getplate() != 999) && 
			(previous.getplate() != plate) && 
			(previous.getplate() != current.getplate())) {
				double delta = 1.0;				
				previous.part(delta,time);
				output.insert(output.end(),previous);
				plate = previous.getplate();
			}
			previous = current;
			
	}

	else {
			double t1 = previous.gettime();
			double t2 = current.gettime();
			Rotation dx = current - previous;
			double delta = ((time - t1)/(t2 - t1));
			dx.part(delta,time);
			Rotation ans = dx + previous;
			output.insert(output.end(),ans);
			plate = current.getplate();
			previous = current;
	}
} // for loop

if  (current.gettime() < time) {
				double delta = 1.0;
				current.part(delta,time);
				output.insert(output.end(),current);
}

return output;

} // giventime()

Rotation Document::getrot(double & time,int & plate, int & cplate) {

/********
	* This method calls the giventime() method
	* finds the two appropriate absolute rotations
	* swaps the plate pairs and adds them
	*
	*******/

list <Rotation> results = giventime(time);
list <Rotation> final;

Rotation current;
int cplateid;
int plateid;

// Check to see if rotation for plate pair already exists

for (list<Rotation>::iterator update = results.begin();
			update != results.end();
			++update) {   

	current = *update;
	plateid = current.getplate();
	cplateid = current.getcplate();


	if ((plate == plateid)&&(cplate == cplateid)) {
		current.toNP();
		return current;
	}

	if ((plate == cplateid)&&(cplate == plateid)) {
		current.swap(); //added to give rotation the correct sense
		current.toNP();
		return current;
	}
}
 
for (list<Rotation>::iterator update = results.begin();
			update != results.end();
			++update) {    // OUTER LOOP
			
		current = *update;
		cplateid = current.getcplate();

		if (cplateid == 000) {

				final.insert(final.end(),current);
				continue;
		}

		list<Rotation>::iterator rotfnd = results.begin();
		int num = 0; // number of searches through list	
	
	while((cplateid != 000) && (num < 2)) {		// INNER LOOP
		
		if (rotfnd == results.end()) {
				rotfnd = results.begin();
				num++;
		}
		Rotation inner = *rotfnd;	

		if (inner.getplate() == cplateid) {
				
				Rotation tmp = current + inner;
				current = tmp; 
				cplateid = current.getcplate();
				num = 0;
		}

			
		rotfnd++;
	}

	if (num < 2) final.insert(final.end(),current);
}

Rotation r1,r2,ans;
bool found1 = false;
bool found2 = false;

for (list<Rotation>::iterator update = final.begin();
			update != final.end();
			++update) {

	Rotation current = *update;
	if (plate == current.getplate()) {
				
				r1 = current;
				if (cplate == 000) {
						r1.swap();
						r1.toNP();
						return r1;
				}
				found1 = true;
	}
	else if (cplate == current.getplate()) {
				
					r2 = current;

				if (plate == 000) {
						r2.toNP();
						return r2;
				}
						 
				
				found2 = true;
	}

}

if (found1 && found2) {
			r1.swap();
			ans = r2 + r1;
			ans.toNP();
}

else {
	cerr << "Warning: Rotation not found\n";
	cerr << "for plate " <<plate <<" and conjugate plate " <<cplate  <<endl;
	cerr << "Check Rotation Hierarchy\n";
	Rotation tmp(0.0,90.0,0.0,999,999,0.0);
	ans = tmp;	
	
	
}

return ans;

} // getrot();

void Document::join() {

/********
	* This method is designed to join any lines with the same header
	* Except if they are further apart than
	* The Lines must be sorted before this method is called
	*
	********/

list <Line> output,input;
list <Line>::iterator getdata2;

for (list <Line>::iterator getdata1 = data.begin();
		getdata1 != data.end();
		++getdata1) {
	input.clear();
	Line current = *getdata1;
	Line next;
	getdata2 = getdata1;
	++getdata2;

	if (getdata2 == data.end()) {
		output.insert(output.end(),current);
		break;
	}
	next = *getdata2; 
	if (next != current) {
		output.insert(output.end(),current);
		continue;
	}
	input.insert(input.end(),current);
	while ((getdata2 != data.end()) && (next == current)) {
		input.insert(input.end(),next);
		if ((++getdata2) != data.end()) next = *getdata2;
	}

	double t = current.gettime();
	int p = current.getplate();
	int cp = current.getcplate();
	Rotation r1 = getrot(t,p,cp);
	t += 5;
	Rotation r2 = getrot(t,p,cp);
	Rotation stage = -r2 + r1;
	list <Line> ans = concatenate(input,stage);
	output.merge(ans);
	getdata1 = --getdata2;
	
 } // for loop
data = output;

} // join()

// Convert linetype to a string
static const char *type2string (Line &l)
{
        switch (l.gettype ()) {
                case 0: return "IS";
                case 1: return "IM";
                case 2: return "CO";
                case 3: return "RI";
                case 4: return "XR";
                default: return "??";
        }
}

list <Line> Document::interpolate(Line &a,Line &b) {

/********
	* This method finds the appropriate finite rotations
	* for the two lines, and calculates the stage rotation
	* then it calls the Line::interpolate() method
	*
	******/

Line young,old;
if (a.gettime() <= b.gettime()) {
	young = a;
	old = b;
}

else {
	young = b;
	old = a;
}	

double ytime,otime;
int yplate,ycplate,oplate,ocplate;

ytime = young.gettime();
yplate = young.getplate();
ycplate = young.getcplate();
otime = old.gettime();
oplate = old.getplate();
ocplate = old.getcplate();
Rotation none(0.0,90.0,0.0,999,999,0.0);
Rotation ryoung = getrot(ytime,yplate,ycplate);
Rotation rold = getrot(otime,oplate,ocplate);
list <Line> ans;
ans.clear();
if ((ryoung == none)||(rold == none)) {
					// Ignore
					// cerr << "No rotation found!\n";
					return ans;
}
	cerr << "Interpolating from " << type2string(old) << "(pl="
		<< old.getplate () << ",conj_pl=" << old.getcplate ()
		<< ",time=" << old.gettime () << "),\n"
		"\t        to " << type2string(young) << "(pl="
		<< young.getplate () << ",conj_pl=" << young.getcplate ()
		<< ",time=" << young.gettime () << "): ";

Rotation diff = -rold + ryoung;
double min, max, avg;
ans = young.interpolate(old,diff, &min, &max, &avg);
	if (!ans.empty ()) {
		fout << "* " << diff << "\n";
		std::cerr << "ok\n" << setprecision(1) << fixed
			<< "\t\tDistance between lines: min=" << min
			<< ", avg=" << avg << ", max=" << max << "\n";
	} else
		std::cerr << "failed\n";

return ans;

}

int Document::reconstruct(double time,int frame,string outputfile) {

/********
	* This method reconstructs all data to the time and fixed plate
	* specified when the Document class is created
	*
	*******/

ofstream fout;
fout.open(outputfile.c_str());
if (fout.bad()) {
		return 1;
}

list <Line> output;
for (list <Line>::iterator recon = data.begin();
	recon != data.end();
	++recon) {
	Line current = *recon;
	int plate = current.getplate();
	double atime = current.gettime();
	double dtime = current.getdtime();
	if (time > atime) continue;
	if (time < dtime) continue;
	Rotation r = getrot(time,frame,plate);
	current.rotate(r);
	output.insert(output.end(),current);
}

ostream_iterator<Line> out(fout,"");
copy(output.begin(),output.end(),out);

return 0;
} // reconstruct()

/**********************************************************************/
/***************** PUBLIC MEMBER FUNCTIONS ****************************/
/**********************************************************************/


int Document::interpolate(){

/********
	*
	* It interpolates between all this isochrons and reconstructs
	* by calling the appropriate methods
	*
	*******/



if (data.begin() == data.end()) {
		cerr << "No Isochrons to interpolate\n";
		return 1;
}

if (data.begin() == --data.end()) {
			cerr << "Only one isochron in data\n";
			return 1;
}
	list <Line> output;
	list <Line>::iterator getdata1 = data.begin();
	for (list <Line>::iterator getdata2 = ++data.begin();
		getdata2 != data.end();
		++getdata2) {
		bool finder = false;
		Line first = *getdata1;
		Line second = *getdata2;
		if (second.gettype() == 1) finder = true;	
		if (first == second) finder = true;
		else {
			list<Line>::iterator getdata3 = getdata2;
			getdata3++;
			if (getdata3 != data.end()) {
					if (second == *getdata3) finder = true;
			}
			getdata3 = getdata1;
			if (getdata3 != data.begin()) {
				getdata3--;
				if (first == *getdata3) finder = true;
			}
			
			
		}	
		if (finder) {

			// Finds the previous isochron to interpolate between
			// in either case where there is a multiple cob segments
			// or multiple isochron segments (called cob below)


			Line cob = second;
			double ctime = cob.gettime();	
			int cobplate = cob.getplate();
			int cobcplate = cob.getcplate();

			Line lastiso = first;
			list <Line>::iterator getlastiso = getdata2;
			getlastiso--;
			for (		;
				getlastiso != data.begin();
				--getlastiso) {

				Line star = *getlastiso;
			
				if ((star.gettype() != 1)&&(star.gettime() < ctime)) {
					lastiso = star;
					break;
				}
			}
			bool found = false;
			while (!found) {
				
				double itime = lastiso.gettime();
				int isoplate = lastiso.getplate();
				int isocplate = lastiso.getcplate();
				Rotation riso = getrot(itime,isoplate,isocplate);
				Rotation rcob = getrot(ctime,cobplate,cobcplate);
				Rotation diff = -rcob + riso;
				found = check(lastiso,cob,diff);
				if (!found) {
						if (getlastiso == data.begin()) found = true;
						else lastiso = *(--getlastiso);
						
				}
			} // while loop
		
		first = lastiso;
		second = cob;
				 
		}
		list <Line> temp;
		double agediff = abs(first.gettime() - second.gettime());
		if ((first.getplate() == second.getplate()) && 
			(first.getcplate() == second.getcplate())&&(agediff < 50) && (!((first.gettype() == 0)&&(second.gettype()==2)))) {
			temp = interpolate(first,second);
			
		}

		else {
		}
		output.merge(temp);
	getdata1++;

	}// for loop
	data.clear();
	data.merge(output);
	// REM reconstruct();
	// REM ostream_iterator<Line> out(fout,"");
	// REM copy(data.begin(),data.end(),out);
	return 0;


} // interpolate
