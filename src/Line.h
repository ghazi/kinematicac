/*
The Line class
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Line.h is part of the Kinematica suite

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Line.h - This file
 *	The main class for handling point data in latitude/longitude
 *      Version 2.0 April 24th, 2013
 *
 **/

#ifndef _LINE_H_
#define _LINE_H_

#include <iostream>
#include <list>
#include <iomanip>
#include <fstream>
#include <cmath>

#include "Point.h"
#include "Rotation.h"
using namespace std;

class Line {

			private:

				// Plates header information

				int region;
				int linenum;
				int plate;
				int cplate;
				double time;
				double dtime;
				enum Linetype { IS , IM, CO, CM , RI, XR } type;
				int colour;
				int number;
				list<Point> data;
			
				// GMT output boolean
	
				bool fisGMToutput;

				// Average Distance
			
				double avedist(Rotation &r);

				// Check number of points

				void check();

             		public:
				// Constructors/Destructor

				Line();
				Line(int r,int lnum,int id1,int id2,double t,double dt,int typ,int col,int n,bool GMT);
				~Line();
				
				// Print Methods

				int getregion() { return region; };
				int getplate() { return plate; };
				int getcplate() { return cplate; };
				double gettime() { return time; };
				double getdtime() { return dtime; };
				int getcolour() { return colour; };
				int gettype() { return (int)type; };

				// List methods
				
				void insert(Point &p);

				// Rotation

				void rotate(Rotation &r);
				// Interpolation		

				list <Line> interpolate(Line &l,Rotation &r,double * mindist, double * maxdist, double *avgdist);

				// Boolean Operators

				bool operator==(Line &m);
				bool operator!=(Line &m);
				bool operator<(Line &m);


				// ---- FRIENDS ---- //
				// Printing

				friend ostream & operator<<(ostream & os, const Line & l); 
				friend list <Line> concatenate(list<Line> &conc,Rotation &stage);
				// Check to see if IS and COB are close

				friend bool check(Line &l,Line &m,Rotation &r);
};

#endif
