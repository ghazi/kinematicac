/*
Getrot - A program for calculating and interpolating rotations
part of the Kinematica suite
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Getrot.h is part of the Getrot program

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Getrot.h - This file
 *	The class for handling the rotation file interaction
 *      Version 2.0 April 24th, 2013
 *
 **/

#ifndef _GETROT_H_
#define _GETROT_H_

#include <fstream>
#include <iostream>
#include <list>
#include <string>
#include <sstream>
#include "Point.h"
#include "Rotation.h"

class Getrot {

	private:

		// Rotation file		
		
		ifstream finrot;

		// Reconstruction time and frame

		double time;
		int plate1;   // Plate pair to use when debugging
		int plate2;	

		// Data lists  

		list <Rotation> rot;

		// Internal Methods

		list <Rotation> giventime(double & time);
		Rotation getrot(double & time,int & plate, int & cplate);	
	public: 
		// Constructor/Destructor

		Getrot(const char * rotfile,const double  t,const int id1,const int id2); 

		~Getrot();

		// Main control program

		int run();
		

};
#endif
