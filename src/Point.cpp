/*
The Point class
Copyright (C) 1999,2001 and 2013 Stuart R. Clark

    Point.cpp is part of the Kinematica suite

    Getrot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *      Point.cpp - This file
 *	The main class for handling point data in latitude/longitude
 *      Version 2.0 April 24th, 2013
 *
 **/


#include "Point.h"
#ifndef rad
#define rad 0.0174532925199432958
#define PI 3.1415926538
#endif

//extern "C" __declspec(dllexport) int adder(int a,int b) {return a+b;};

/**********************************************************************/
/***************** CONSTRUCTORS/DESTRUCTORS ***************************/
/**********************************************************************/


Point::Point() {

	lon = 0.0;
	lat = 0.0;
	age = 0.0;
	spreadrate = 0.0;
	spreaddir = 0.0;
	spreadasym = 0.0;
	fisXYoutput = true;
	fisGMTPoint = false;
}

Point::Point(double X,double Y) {
	
	lon = X;
	if (lon >= 360) lon -= 360;
	if (lon <  0)   lon += 360;	
	lat = Y;
	if (lat > 90) lat = 90;
	if (lat < -90) lat = -90;
	age = 0.0;
	spreadrate = 0.0;
	spreaddir = 0.0;
	spreadasym = 0.0;
	fisXYoutput = false;
	fisGMTPoint = false;

}

Point::Point(double X,double Y,double t,double sr,double sd,double sa) {

	lon = X;
	if (lon >= 360) lon -= 360;
	if (lon <  0)   lon += 360;	
	lat = Y;
	if (lat > 90) lat = 90;
	if (lat < -90) lat = -90;
	age = t;
	spreadrate = sr;
	spreaddir = sd;
	spreadasym = sa;
	fisGMTPoint = true;
	fisXYoutput = false;
}

/**********************************************************************/
/***************** PUBLIC MEMBER FUNCTIONS ****************************/
/**********************************************************************/

double Point::vb(double x) const {

	const double a = 0.78539816;
	const double b = 0.0067226;

	double ans = log(tan(a + 0.5 * x)) - b * sin(x);

	return ans;
}

void Point::shift() {

	lon -= 360;

}


double Point::dist(Point const &p) const {

// Haversine formula 

double R,dLat,dLon,lat1,lat2,a,c,d;

R = 6371; // km
dLat = (lat - p.lat)*rad;
dLon = (lon-p.lon)*rad;
lat1 = lat * rad;
lat2 = p.lat * rad;

a = sin(dLat/2) * sin(dLat/2) +
        sin(dLon/2) * sin(dLon/2) * cos(lat1) * cos(lat2); 
c = 2 * atan2(sqrt(a), sqrt(1-a)); 
d = R * c; // in km

return d;

/* Law of Cosines
double dlat,favb,dlovb,distance;

dlat = lat - p.lat;

if (abs(dlat) < 0.01) {

	favb = cos( 0.5 * rad * (lat + p.lat));

}

else {

	double t1,t2;
	
	if (p.lat != 0.0) t1 = vb(p.lat * rad);
	else t1 = 0.0;

	if (lat != 0.0)	t2 = vb(lat * rad);
	else t2 = 0.0;

	if (t1 == t2) favb = 0.0;
	else favb = (dlat * rad) / (t1 - t2);
}

dlovb = favb * (p.lon - lon);
distance = 60 * sqrt(dlat * dlat + dlovb * dlovb); // nautical miles
return distance;
*/
}

double Point::cour(Point const &p) const {

	double dlon,a,c;

	dlon = p.lon - lon;

	a = (vb(p.lat*rad) - vb(lat*rad))/rad;

	if (a != 0.0) c = atan2(dlon,a);

	else {
		c = 2* PI;
		if (dlon < 0) c = -c;
	}

	if (c < 0) c += 2 * PI;

	c = c/rad;
	return c;
}

void Point::equals(Point const &p) {

	lat = p.lat;
}


void Point::update(double t,double sr,double sd,double sa) {

	fisGMTPoint = true;
	age = t;
	spreadrate = sr;
	spreaddir = sd;
	spreadasym = sa;
}
	
void Point::update(Point &p) {

	fisGMTPoint = p.fisGMTPoint;	
	fisXYoutput = p.fisXYoutput;
	age = p.age; 
	spreadrate = p.spreadrate;
	spreaddir = p.spreaddir;
	spreadasym = p.spreadasym;
}

bool Point::operator==(Point const &p) const {

	if ((lon == p.lon) && (lat == p.lat)) return true;
	return false;

}

bool Point::operator<(Point const &p) const {

	if (lat > p.lat) return true;

return false;

}

/**********************************************************************/
/*************************** FRIENDS  *********************************/
/**********************************************************************/

ostream & operator<<(ostream & os, const Point &p) {

    if (!p.isXYoutput()) {
	os.setf(ios::showpoint);
	os.setf(ios::right,ios::adjustfield);
	os.setf(ios::fixed,ios::floatfield);
	os.width(9);
	os.precision(4);
	os << p.getlat(); 
	os.setf(ios::right,ios::adjustfield);
	os.width(10);
	os << p.getlon(); 
	return os;
     }
     else {
	os.setf(ios::showpoint);
	os.precision(4);
	os << p.getlon() << " " << p.getlat();

	if (p.isGMTPoint()) {
	os.precision(4);
	os << " " << p.getage() << " " << p.getspreadrate();
	os.precision(5); 
	os << " " << p.getspreaddir() << " " << p.getspreadasym();
    	}
     			
     }
return os;
} 
